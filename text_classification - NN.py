import random
import numpy as np
import pandas as pd 
import gensim.models.word2vec as wv

from nltk.stem.snowball import SnowballStemmer
from sklearn.pipeline import Pipeline
import numpy as np
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import StandardScaler
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import SGDClassifier
from sklearn.neural_network import MLPClassifier



###################################################################
print("Herunterladen des Datensatzes")
###################################################################

with open("train-neg.txt", "r") as neg:
    neg_array = np.array([])
    for line in neg:
        neg_array = np.append(neg_array, line)

with open("train-pos.txt", "r") as pos:
    pos_array = np.array([])
    for line in pos:
        pos_array = np.append(pos_array, line)


###################################################################
print("Zusammenfügen und Durchmischen des Datensatzes sowie Preprocessing")
###################################################################

help_array =np.array([])
for i in range(3000):
    help_array = np.append(help_array, np.int(i))
random.shuffle(help_array)

help_array = help_array.astype(int)



transition_target = np.array([])
transition_target = np.append(np.zeros(len(neg_array)), np.ones(len(pos_array)))


transition_data = np.array([])
transition_data = np.append(neg_array, pos_array)

stemmer = SnowballStemmer(language="english" , ignore_stopwords=True)
transition_data = [" ".join([stemmer.stem(word) for word in sentence.split(" ")]) for sentence in transition_data]
transition_data = [sentence.split("\n ")for sentence in transition_data]
transition_data = np.array(transition_data)

target = np.array([])
data = np.array([])
for j in range(3000):
    target = np.append(target, transition_target[help_array[j]])
    data = np.append(data, transition_data[help_array[j]])

concatenated_array = np.vstack([[data],[target]])


###################################################################
print("Trainieren des Models und Evaluation")
###################################################################

text_clf_svm = Pipeline([('vect', CountVectorizer(stop_words='english')), ('tfidf', TfidfTransformer()), ('clf-nn', MLPClassifier())])

_ = text_clf_svm.fit(concatenated_array[0][:2000], concatenated_array[1][:2000])


predicted = text_clf_svm.predict(concatenated_array[0][2001:])
result = np.mean(predicted == concatenated_array[1][2001:])
print(result)

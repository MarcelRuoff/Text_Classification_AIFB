import numpy as np
import csv
from sklearn.pipeline import Pipeline
import lemmatizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import Normalizer
from sklearn.model_selection import GridSearchCV

neg_array = np.array([])
pos_array = np.array([])
help_array = np.array([])
transition_target = np.array([])
target = np.array([])
transition_data = np.array([])
data = np.array([])

parameters = {
    'tfidf__max_df': (0.5, 0.75, 1.0),
    'tfidf__max_features': (None, 5000, 10000, 50000),
    'tfidf__use_idf': (True, False),
    'tfidf__norm': ('l1', 'l2'),
    'clf-svm__alpha': (0.00001, 0.000001),
    'clf-svm__penalty': ('l2', 'elasticnet'),
    # 'clf-svm__n_iter': (10, 50, 80),
    'clf-svm__loss': ('hinge', 'log', 'modified_huber'),
    'lsa__n_components': (100, 10000, 20000, 50000)
}

text_clf_svm = Pipeline([('tfidf', TfidfVectorizer()),
                         ('lsa', TruncatedSVD(n_components=10000)),
                         ('norm', Normalizer()),
                         ('clf-svm', SGDClassifier(loss="hinge", penalty='l2', learning_rate="optimal", n_iter=1000))])

estimator = GridSearchCV(text_clf_svm, parameters, n_jobs=4, verbose=1)

if __name__ == '__main__':
    ###################################################################
    print("Herunterladen des Datensatzes")
    ###################################################################

    positive_examples = list(open("train-neg.txt", "r").readlines())
    positive_examples = [s.strip() for s in positive_examples]
    negative_examples = list(open("train-pos.txt", "r").readlines())
    negative_examples = [s.strip() for s in negative_examples]
    test_data = list(open("test.txt", "r").readlines())
    test_data = [s.strip() for s in test_data]

    # Split by words
    x_text = positive_examples + negative_examples

    ###################################################################
    print("Hinzufuegen von Labels")
    ###################################################################

    positive_labels = [1 for _ in positive_examples]
    negative_labels = [0 for _ in negative_examples]
    y = np.concatenate([positive_labels, negative_labels], 0)

    ###################################################################
    print("Zusammenfuegen und Durchmischen des Datensatzes sowie Preprocessing")
    ###################################################################

    concatenated_array = np.vstack([[x_text], [y]])

    concatenated_array = np.array(concatenated_array)
    shuffle_indices = np.random.permutation(np.arange(len(concatenated_array[0])))
    transition_shuffled_data = np.array(concatenated_array[0][shuffle_indices])
    transition_shuffled_target = np.array(concatenated_array[1][shuffle_indices])

    shuffled_data = transition_shuffled_data.tolist()
    shuffled_target = transition_shuffled_target.tolist()

    shuffled_data_stem = list()

    preprocessing_data = shuffled_data + test_data

    for sentence in preprocessing_data:
        shuffled_data_stem.append(lemmatizer.stem(sentence))

    print(len(shuffled_data_stem))
    ###################################################################
    print("Trainieren des Models und Evaluation")
    ###################################################################

    for i in range(0, 3000, 300):

        print("Neues Model")

        training_data = list()
        for j in range(i):
            training_data.append(shuffled_data_stem[j])
        for j in range((i + 300), 3000):
            training_data.append(shuffled_data_stem[j])

        training_target = list()
        for j in range(i):
            training_target.append(shuffled_target[j])
        for j in range((i + 300), len(shuffled_target)):
            training_target.append(shuffled_target[j])

        validation_data = list()
        for j in range(i, (i + 300)):
            validation_data.append(shuffled_data_stem[j])

        validation_target = list()
        for j in range(i, (i + 300)):
            validation_target.append(shuffled_target[j])

        test_data_prep = shuffled_data_stem[3000:]

        print(len(training_data))
        print(len(validation_data))
        _ = estimator.fit(training_data, training_target)

        predicted = estimator.predict(validation_data)
        result = np.mean(predicted == validation_target)
        print(result)

        print(len(test_data_prep))
        predictedResults = estimator.predict(test_data_prep)

        print(len(predictedResults))

        with open((str(i) + 'result.csv'), "w") as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=' ',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(result)
            for k in range(len(predictedResults)):
                spamwriter.writerow(predictedResults[k])

    x = input('Beenden?')

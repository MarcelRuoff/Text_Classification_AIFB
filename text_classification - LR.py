import random
import numpy as np
from nltk.stem.snowball import SnowballStemmer
from nltk.stem import WordNetLemmatizer
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import LogisticRegression


###################################################################
print("Herunterladen des Datensatzes")
###################################################################

with open("train-neg.txt", "r") as neg:
    neg_array = np.array([])
    for line in neg:
        neg_array = np.append(neg_array, line)

with open("train-pos.txt", "r") as pos:
    pos_array = np.array([])
    for line in pos:
        pos_array = np.append(pos_array, line)


###################################################################
print("Zusammenfügen und Durchmischen des Datensatzes sowie Preprocessing")
###################################################################

help_array =np.array([])
for i in range(3000):
    help_array = np.append(help_array, np.int(i))
random.shuffle(help_array)

help_array = help_array.astype(int)



transition_target = np.array([])
transition_target = np.append(np.zeros(len(neg_array)), np.ones(len(pos_array)))


transition_data = np.array([])
transition_data = np.append(neg_array, pos_array)

stemmer = SnowballStemmer(language="english" , ignore_stopwords=True)
lemmatizer = WordNetLemmatizer()
transition_data = [" ".join([stemmer.stem(word) for word in sentence.split(" ")]) for sentence in transition_data]
transition_data = [sentence.split("\n ")for sentence in transition_data]
transition_data = np.array(transition_data)

target = np.array([])
data = np.array([])
for j in range(3000):
    target = np.append(target, transition_target[help_array[j]])
    data = np.append(data, transition_data[help_array[j]])

concatenated_array = np.vstack([[data],[target]])

print(concatenated_array[0][:5])


###################################################################
print("Trainieren des Models und Evaluation")
###################################################################

text_clf_svm = Pipeline([('vect', CountVectorizer(stop_words='english')), ('clf-lr', SGDClassifier(loss="hinge", penalty='l2', learning_rate="optimal" , n_iter=1000, ))])


edges = [300, 600, 900, 1200, 1500, 1800, 2100, 2400, 2700, 3000]

split_arrays = np.split(concatenated_array, edges)

for i in range(len(split_arrays)):

    training_data = np.array([concatenated_array[0][:i], concatenated_array[0][(i+300):]])
    training_target = np.array([concatenated_array[1][:i], concatenated_array[1][(i+300):]])

    test_data = np.array([concatenated_array[0][i:(i+300)]])
    test_target = np.array([concatenated_array[1][i:i+300]])

    print(training_data.shape)
    print(training_target.shape)

    _ = text_clf_svm.fit(training_data, training_target)

    predicted = text_clf_svm.predict(test_data)
    result = np.mean(predicted == test_target)
    print(result)
